"""
Objective 4 - Find dll
"""
import shutil
from datetime import datetime

def waitFindText(string, interval = 5):
    try:
        wait(interval)
        res = findText(string)
        return res
    except:
        print("[log] Text Not Found : "+string)
        return None 

def waitImage(image, interval = 5):
    try:
        res = wait(image,interval)
        return res
    except:
        print("[log] Image Not Found")
        return None

def waitImageVanish(image):
    try:
        res = waitVanish(image, FOREVER)
        return res
    except:
        print("[log] Image Found")
        return None

def clickAndWait(thing):
    click(thing)
    wait(0.05)

app = App.open("C:/Users/user/Desktop/ProcessExplorer/procexp.exe")

titlebar = waitImage("1609118705549.png")
clickAndWait(titlebar)

menu = waitImage(Pattern("1609132298150.png").similar(0.85).targetOffset(19,-1))
clickAndWait(menu)

menu_sys_info = waitImage(Pattern("1609131151767.png").targetOffset(-82,-1))
clickAndWait(menu_sys_info)

window_sys_info = waitImage(Pattern("1609131179705.png").similar(0.87))
clickAndWait(window_sys_info)

search_area = waitImage(Pattern("1609131325661.png").similar(0.92).targetOffset(89,-2))
search_button = waitImage("1609131410612.png")
clickAndWait(search_area)

type("python")

clickAndWait(search_button)
wait(1)

waitImageVanish(Pattern("1609131561246.png").similar(0.90))

process_list = waitImage(Pattern("1609131325661.png").similar(0.92).targetOffset(-23,54))
clickAndWait(process_list)

print("[log] Find dll")
