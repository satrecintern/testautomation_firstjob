"""
Objective 1 - Open the Help Site
"""

def waitFindText(string, interval = 5):
    try:
        wait(interval)
        res = findText(string)
        return res
    except:
        print("[log] Text Not Found : "+string)
        return None 

def waitImage(image, interval = 5):
    try:
        res = wait(image,5)
        return res
    except:
        print("[log] Image Not Found")
        return None

def clickAndWait(thing):
    click(thing)
    wait(0.05)
    
app = App.open("C:/Users/user/Desktop/ProcessExplorer/procexp.exe")
titlebar = waitImage("1609118705549.png")
clickAndWait(titlebar)

menu = waitImage(Pattern("1609122186741.png").similar(0.93).targetOffset(144,-1))
clickAndWait(menu)
help_menu = waitImage("1609122218903.png")
clickAndWait(help_menu)
wait(1)
help_window = waitImage("1609122298233.png")
clickAndWait(help_window)
#help_url = findLocationfromText("www") #not working (underlined text)
help_url = waitImage("1609123253618.png")
clickAndWait(help_url)
result_page = waitFindText("Windows Sysinternals")

if result_page is not None:
    print("[log] Passed : Objective 1 - Open the Help Site")
else:
    print("[log] Failed : Objective 1 - Open the Help Site")