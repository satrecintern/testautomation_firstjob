"""
Objective 5 - Make Mini Dump # not well
"""

def waitFindText(string, interval = 5):
    try:
        region.wait(interval)
        res = region.findLine(string)
        return res
    except:
        print("[log] Text Not Found : "+string)
        return None 

def waitImage(image, interval = 5):
    try:
        res = region.wait(image,interval)
        return res
    except:
        print("[log] Image Not Found")
        return None

def waitImageVanish(image):
    try:
        res = region.waitVanish(image, FOREVER)
        return res
    except:
        print("[log] Image Found")
        return None

def clickAndWait(thing):
    region.click(thing)
    wait(0.05)

def findObjectiveProcess(object_process_name):
    res = waitFindText(object_process_name,0.1)
    while res is None:
        if region.exists(Pattern("downscroll.png").similar(0.95)):
            break
        type(Key.PAGE_DOWN)
        res = waitFindText(object_process_name,0.1)
    return res

app = App.open("C:/Users/user/Desktop/ProcessExplorer/procexp.exe")

titlebar = waitImage("1609118705549.png")


region = App.focusedWindow()
clickAndWait(titlebar)

menu = waitImage(Pattern("1609132298150.png").similar(0.85).targetOffset(-24,-1))

process = findObjectiveProcess("notepad.exe")

clickAndWait(process)


clickAndWait(menu)

menu_create_dump = waitImage(Pattern("1609133152077.png").similar(0.88))
mouseMove(menu_create_dump)

menu_create_mini_dump = waitImage(Pattern("1609133254475.png").similar(0.90))
clickAndWait(menu_create_mini_dump)

save_button = waitImage(Pattern("1609133358398.png").similar(0.91))
clickAndWait(save_button)

print("[log] save Dump")
