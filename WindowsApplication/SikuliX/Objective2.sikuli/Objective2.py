"""
Objective 2 - Capture the CPU state
"""
import shutil
from datetime import datetime

def waitFindText(string, interval = 5):
    try:
        wait(interval)
        res = findText(string)
        return res
    except:
        print("[log] Text Not Found : "+string)
        return None 

def waitImage(image, interval = 5):
    try:
        res = wait(image,5)
        return res
    except:
        print("[log] Image Not Found")
        return None

def clickAndWait(thing):
    click(thing)
    wait(0.05)

app = App.open("C:/Users/user/Desktop/ProcessExplorer/procexp.exe")

titlebar = waitImage("1609118705549.png")
clickAndWait(titlebar)

menu = waitImage(Pattern("1609122186741.png").similar(0.90).targetOffset(-60,-1))
clickAndWait(menu)

menu_sys_info = waitImage(Pattern("1609129057601.png").targetOffset(-127,0))
clickAndWait(menu_sys_info)

window_sys_info = waitImage("1609129118922.png")
clickAndWait(window_sys_info)
#app = App.focus("System Information")

window_app = App("System Information").window()
res = capture(window_app)
focusWindow = App.focusedWindow()
regionImage = capture(focusWindow)

#type(Key.PRINTSCREEN, KEY_ALT)
#regionImage = Env.getClipboard()


today = str(datetime.today()).split()[0]
shutil.move(regionImage, 'C:/Users/user/Desktop/'+today+'CPU_상태캡처.png')
print("[log] Capture System Information Screen")
