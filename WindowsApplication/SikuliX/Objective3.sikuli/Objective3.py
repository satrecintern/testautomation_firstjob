"""
Objective 3 - Kill the Process
"""

def waitFindText(string, interval = 5):
    try:
        wait(interval)
        res = findLine(string)
        return res
    except:
        print("[log] Text Not Found : "+string)
        return None 

def waitImage(image, interval = 5):
    try:
        res = wait(image,interval)
        return res
    except:
        print("[log] Image Not Found")
        return None

def clickAndWait(thing):
    click(thing)
    wait(0.05)

def findObjectiveProcess(object_process_name):
    res = waitFindText(object_process_name,0.5)
    while res is None:
        if exists(Pattern("downscroll.png").similar(0.95)):
            break
        type(Key.PAGE_DOWN)
        res = waitFindText(object_process_name,0.5)
    return res

app_toclose = App.open("C:/Windows/System32/notepad.exe")
app = App.open("C:/Users/user/Desktop/ProcessExplorer/procexp.exe")
wait("1609118368410.png")
titlebar = wait("1609118705549.png")
clickAndWait(titlebar)    
    
res = findObjectiveProcess("notepad.exe")
    
if res is None:
    print("[log] Process Not Found")
else:
    rightClick(res)
    kill_process = waitImage("1609118975167.png")
    click(kill_process)
    
    kill_alert = waitImage(Pattern("1609120736049.png").targetOffset(-43,0))
    click(kill_alert)
    denied_alert = waitImage(Pattern("1609119914055.png").targetOffset(118,53)) 
    if kill_alert is not None:
        click(denied_alert)