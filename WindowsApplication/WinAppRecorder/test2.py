import io

import pyautogui
from PIL import Image
from appium import webdriver
from selenium.webdriver.common.keys import Keys

class Test():
    def setup_method(self,method):
        desired_caps={}
        desired_caps['app'] = 'Root'
        desired_caps['platformName'] = 'Windows'
        desired_caps['deviceName'] = 'WindowsPC'
        self.driver = webdriver.Remote(command_executor='http://127.0.0.1:4723', desired_capabilities = desired_caps)

    def teardown_method(self,method):
        self.driver.quit()

    def test(self):

        # LeftClick on MenuItem "View" at (31,7)
        print("LeftClick on MenuItem \"View\" at (31,7)")
        xpath_LeftClickMenuItemView_31_7 = "/Pane[@ClassName=\"#32769\"][@Name=\"데스크톱 1\"]/Window[@ClassName=\"PROCEXPL\"]/MenuBar[@Name=\"응용 프로그램\"][@AutomationId=\"MenuBar\"]/MenuItem[@Name=\"View\"]"
        winElem_LeftClickMenuItemView_31_7 = self.driver.find_element_by_xpath(xpath_LeftClickMenuItemView_31_7)
        winElem_LeftClickMenuItemView_31_7.click()


        # LeftClick on MenuItem "System Information..." at (27,8)
        print("LeftClick on MenuItem \"System Information...\" at (27,8)")
        xpath_LeftClickMenuItemSystemInfo_27_8 = "/Pane[@ClassName=\"#32769\"][@Name=\"데스크톱 1\"]/Window[@ClassName=\"PROCEXPL\"]/Menu[@ClassName=\"#32768\"][@Name=\"View\"]/MenuItem[@Name=\"System Information...\"]"
        winElem_LeftClickMenuItemSystemInfo_27_8 = self.driver.find_element_by_xpath(xpath_LeftClickMenuItemSystemInfo_27_8)
        winElem_LeftClickMenuItemSystemInfo_27_8.click()


        # LeftClick on TitleBar "" at (496,16)
        print("LeftClick on TitleBar \"\" at (496,16)")
        xpath_LeftClickTitleBar_496_16 = "/Pane[@ClassName=\"#32769\"][@Name=\"데스크톱 1\"]/Window[@ClassName=\"#32770\"][@Name=\"System Information\"]/TitleBar[@AutomationId=\"TitleBar\"]"
        winElem_LeftClickTitleBar_496_16 = self.driver.find_element_by_xpath(xpath_LeftClickTitleBar_496_16)
        winElem_LeftClickTitleBar_496_16.click()

        # hard coded
        xpath_LeftClickTitleBar_496_16 = "/Pane[@ClassName=\"#32769\"][@Name=\"데스크톱 1\"]/Window[@ClassName=\"#32770\"][@Name=\"System Information\"]"
        winElem_LeftClickTitleBar_496_16 = self.driver.find_element_by_xpath(xpath_LeftClickTitleBar_496_16)
        image = winElem_LeftClickTitleBar_496_16.screenshot_as_png
        imageStream = io.BytesIO(image)
        im = Image.open(imageStream)
        im.save('test.png')
