# Generated by Selenium IDE
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestObjective5():
  def setup_method(self, method):
    self.driver = webdriver.Chrome()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_objective5(self):
    self.driver.get("http://localhost:8080/owners/12")
    self.driver.set_window_size(1149, 853)
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("Apple01")
    self.driver.find_element(By.ID, "birthDate").click()
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-01")
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple02")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-02")
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'cat']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple03")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-03")
    self.driver.find_element(By.CSS_SELECTOR, ".has-feedback").click()
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'dog']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple04")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-04")
    self.driver.find_element(By.CSS_SELECTOR, ".form-group:nth-child(4)").click()
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'hamster']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple05")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-05")
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'lizard']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple06")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-06")
    self.driver.find_element(By.CSS_SELECTOR, ".has-feedback").click()
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'snake']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple07")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-07")
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple08")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-08")
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'cat']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple08")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-08")
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'cat']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple09")
    element = self.driver.find_element(By.ID, "birthDate")
    actions = ActionChains(self.driver)
    actions.move_to_element(element).click_and_hold().perform()
    element = self.driver.find_element(By.ID, "birthDate")
    actions = ActionChains(self.driver)
    actions.move_to_element(element).perform()
    element = self.driver.find_element(By.ID, "birthDate")
    actions = ActionChains(self.driver)
    actions.move_to_element(element).release().perform()
    self.driver.find_element(By.ID, "birthDate").click()
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-09")
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'dog']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn:nth-child(4)").click()
    self.driver.find_element(By.ID, "name").click()
    self.driver.find_element(By.ID, "name").send_keys("apple10")
    self.driver.find_element(By.ID, "birthDate").send_keys("2000-01-10")
    self.driver.find_element(By.ID, "type").click()
    dropdown = self.driver.find_element(By.ID, "type")
    dropdown.find_element(By.XPATH, "//option[. = 'hamster']").click()
    self.driver.find_element(By.ID, "type").click()
    self.driver.find_element(By.CSS_SELECTOR, ".btn").click()
  
